

from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
from scipy.stats import weibull_min
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy

# read in the Excel files for different pulse energies
pulse_intensity = [2500, 2000, 1500, 1250, 1000]
pulse_energies = []

for intensity in pulse_intensity:
    energy = (4E-12*intensity**4 - 1E-07*intensity**3 +
              0.0004*intensity**2+0.139*intensity-146.13)
    pulse_energies.append(energy)
filenames = ['energy_2500.csv',
             'energy_2000.csv', 'energy_1500.csv', 'energy_1250.csv', 'energy_1000.csv']
cross_section_values = []

for energy, filename in zip(pulse_energies, filenames):
    data = pd.read_csv(filename, header=None, delimiter=';')

    # convert voltage values to latchup probabilities
    max_latchup_voltage = 100
    num_latchups = np.count_nonzero((data < max_latchup_voltage) & (data > 0))
    latchup_prob = np.where(data < max_latchup_voltage, 1, 0)

    # calculate the effective area of the device
    x_step, y_step = 0.25, 0.25  # in micrometers
    rows, cols = data.shape
    pixel_area = x_step * y_step  # in square micrometers
    effective_area = cols * rows * pixel_area

    # calculate the cross section for this energy
    energy_prob = np.where(data < energy, 1, 0)
    energy_prob_sum = np.sum(np.multiply(energy_prob, latchup_prob))
    cross_section = energy_prob_sum * pixel_area

    # add the cross section to the list of cross sections
    cross_section_values.append(cross_section)
    print(f"Energy: {energy}, Cross section: {np.mean(cross_section)}")
# plot the data
fig1, ax1 = plt.subplots(figsize=(6, 4), dpi=200)
ax1.scatter(pulse_energies, cross_section_values)

# set axis labels and title
ax1.set_xlabel('Pulse Energy (pJ)')
ax1.set_ylabel('SEL sensitive area ($\mu$m$^2$)')
ax1.set_title('Latchup Cross Section vs Pulse Energy')
ax1.grid(color='gray', linestyle='--', linewidth=0.5)
ax1.set_ylim(min(cross_section_values)-5, max(cross_section_values)+10)

plt.show()
