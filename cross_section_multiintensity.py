# Import Necessary Libraries

from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
from scipy.stats import weibull_min
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os

# define the data folder path
data_folder = r"C: \Users\"

# get a list of all CSV files in the data folder
csv_files = [f for f in os.listdir(data_folder) if f.endswith('.csv')]

# loop over each CSV file in the folder and generate a cross-section plot
for csv_file in csv_files:

    # read in the data from the CSV file
    csv_path = os.path.join(data_folder, csv_file)
    # Change delimeter based on your seperated symbols
    data = pd.read_csv(csv_path, header=None, delimiter=';')

    # convert ADC laser intensity values to pulse energies in pJ
    # This conversion is at 10 ns pulse width for DLTM 2
    data = 4E-12*data**4 - 1E-07*data**3 + 0.0004*data**2+0.139*data-146.13
    # determine the maximum latchup energy value in data set which should not be considered as latchup data
    max_latchup_energy = data.max().max()

    # calculate number of rows and columns in the excel sheet to determine the scan resolution
    rows, cols = data.shape
    x_step, y_step = 0.25, 0.25  # in micrometers
    scan_resolution = cols*x_step, rows*y_step
    print(f"Scan resolution: {scan_resolution}")

    # set pixel area (chip specification)
    pixel_area = x_step * y_step  # in square micrometers

    # count the number of latchup events
    num_latchups = np.count_nonzero(data < max_latchup_energy)

    # calculate the latchup probability for each cell
    latchup_prob = np.where(data < max_latchup_energy, 1, 0)

    # calculate the effective area of the device
    effective_area = cols * rows * pixel_area

    # calculate the pulse energy values from the data set
    pulse_energies = data.values.flatten()

    # calculate the cross section for each pulse energy value
    cross_section = []

    for energy in pulse_energies:
        energy_prob = np.where(data < energy, 1, 0)
        energy_prob_sum = np.sum(np.multiply(energy_prob, latchup_prob))
        cross_section.append(energy_prob_sum * pixel_area)

    # Uncomment this to limit the number of data points to 1000 if there are many data points in graph
    # pulse_energies = pulse_energies[::int(len(pulse_energies)/1000)]
    # cross_section = cross_section[::int(len(cross_section)/1000)]

    # plot the data
    fig1, ax1 = plt.subplots(figsize=(6, 4), dpi=200)

    ax1.scatter(pulse_energies, cross_section, s=5)

    # set axis labels and title
    ax1.set_xlabel('Pulse Energy (pJ)', fontsize=12)
    ax1.set_ylabel('SEL sensitive area  ($\mu$m$^2$)', fontsize=12)
    ax1.set_title(
        f'Latchup Cross Section vs Pulse Energy ({csv_file})', fontsize=12)
    ax1.grid(color='gray', linestyle='--', linewidth=0.5)
    ax1.tick_params(axis='both', which='major', labelsize=10)
    plt.show()
